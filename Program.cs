﻿using System;


namespace NumberGuesser
{
    //Main class
    class Program
    {
        //Entry point method
        static void Main(string[] args)
        {
            //get info
            GetAppInfo();
            string inputUser;
            //ask for user
            inputUser=GreetUser();
      
            while (true)
            {
                Console.WriteLine("Hello {0}, let's play a game", inputUser);
                //set correct number
                //int correctNumber = 7;

                //Create a new random object
                Random random = new Random();
                int correctNumber = random.Next(1, 10);
                //Init guess var
                int guess = 0;
                //aks user for number
                Console.WriteLine("Guess a number between 1 and 10");
                while (guess != correctNumber)
                {

                    string input = Console.ReadLine();
                    //make sure it is a number
                    if (!int.TryParse(input, out guess))
                    {
                        //print error message
                        PrintColorMessage(ConsoleColor.Red, "Please Use an actual number");
                        //keep going
                        continue;
                    }
                    guess = Int32.Parse(input);

                    if (guess != correctNumber)
                    {
                        PrintColorMessage(ConsoleColor.Red, "Number is incorrect, try diffrent number");
                    }
                }
                PrintColorMessage(ConsoleColor.Yellow, "Great You Are correct");

                //Ask to play again
                Console.WriteLine("Play agai? [Y or N]");
                //get answer
                string answer = Console.ReadLine().ToUpper();
                if(answer =="Y")
                {
                    continue;
                }
                else if (answer =="N")
                {
                    return;
                }
                else
                {
                    return;
                }
            }
        }
        static void GetAppInfo()
        {
            //string name = "Jakub Zaton";
            //int age = 21;
            //Console.WriteLine(name+" Is "+age);
            //Console.WriteLine("{0} is {1}",name,age);

            //App vars
            string appName = "Number Guesser";
            string appAuthor = "Jakub Zaton";

            //Change text color
            Console.ForegroundColor = ConsoleColor.Green;
            //info about an app
            Console.WriteLine("{0}: Made by {1}", appName, appAuthor);
            //Reset color
            Console.ResetColor();
        }
        static string GreetUser()
        {
            //Ask username
            Console.WriteLine("What is Your name?");
            string inputUser = Console.ReadLine();
            return inputUser;
        }

        //Print color message
        static void PrintColorMessage(ConsoleColor color, string message)
        {
            Console.ForegroundColor = color;
            //Tell usr it is wrong nimber
            Console.WriteLine(message);
            //Reset color
            Console.ResetColor();
        }
    }
}
